//
// Created by Matteo Favaro on 21/12/2016.
//

#ifndef MOTIONSENSOR_H
#define MOTIONSENSOR_H


class MotionSensor : public Sensor {
 public:
  MotionSensor(uint16_t const address,
               uint8_t const id);
  bool prepareDataMessageToSend(MessageHelper *message);
  bool update();
  bool hasToSend();
 private:
  bool sensorChildrenPresentationReceived(MessageHelper *message);
  bool sensorParentPresentationReceived(MessageHelper *message);
  bool setSensorInfo(MessageHelper *message);
  bool getSensorInfo(MessageHelper *message);
  bool systemFuncion(MessageHelper *message);
  bool incomingDataIsAStream(MessageHelper *message);

};


#endif //MOTIONSENSOR_H
