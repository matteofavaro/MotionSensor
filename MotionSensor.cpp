//
// Created by Matteo Favaro on 21/12/2016.
//

#include "MotionSensor.h"

MotionSensor::MotionSensor(uint16_t const address,
                           uint8_t const id) :
    Sensor(address,
           id,
           S_MOTION,
           V_MOTION) {
}

bool MotionSensor::prepareDataMessageToSend(MessageHelper *message) {
}
bool MotionSensor::update() {
}
bool MotionSensor::hasToSend() {
}
bool MotionSensor::sensorParentPresentationReceived(MessageHelper *message) {
}

bool MotionSensor::sensorChildrenPresentationReceived(MessageHelper *message) {
  return false;
}
bool MotionSensor::setSensorInfo(MessageHelper *message) {
  return false;
}
bool MotionSensor::getSensorInfo(MessageHelper *message) {
  return false;
}
bool MotionSensor::systemFuncion(MessageHelper *message) {
  return false;
}
bool MotionSensor::incomingDataIsAStream(MessageHelper *message) {
  return false;
}
